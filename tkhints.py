#!/usr/bin/env python3
"""
The GPLv3 License (GPLv3)

Copyright (c) 2023 Yehoward Rudenco <rudencoyehoward@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import sys
import subprocess
from pathlib import Path

import tkinter as tk

import json
from typing import NamedTuple 


class HintsError(Exception):
    pass

class RGB(NamedTuple):
    red: int
    green: int
    blue: int

def to_rgb(html_color: str) -> RGB:
    """
    Transforms an html style color to an RGB object.

    @param html_color: A color in format #RRGGBB
    """
    hex_val = html_color.removeprefix("#")
    hex_red = hex_val[0:2]
    hex_gre = hex_val[2:4]
    hex_blu = hex_val[4:]
    return RGB(
        int(hex_red, 16),
        int(hex_gre, 16),
        int(hex_blu, 16),
    )

def add_rgb(rgb1: RGB, rgb2: RGB) -> RGB:
    """A functiton that add two RGB values and returns a new RGB object."""
    diff_r, diff_g, diff_b = (
        rgb1.red + rgb2.red,
        rgb1.green + rgb2.green,
        rgb1.blue + rgb2.blue,
    )
    return RGB(
        diff_r if diff_r > 0 else 0,
        diff_g if diff_g > 0 else 0,
        diff_b if diff_b > 0 else 0,
    )

def rgb_to_html(rgb: RGB) -> str:
    """Converts the RGB object to a html color string"""
    return f"#{rgb.red:02X}{rgb.green:02X}{rgb.blue:02X}"



def get_wal_colors():
    wal_path = Path("~/.cache/wal/colors.json").expanduser()
    if wal_path.exists():
        with open(wal_path, "r") as f:
            wal_dic = json.load(f)
    else:
        wal_dic = {}

    colors = wal_dic.get("colors", {})
    bg = colors.get("color0") or "#000"
    rgbg = to_rgb(bg)
    title_bg = rgb_to_html(add_rgb(rgbg, RGB(10, 10, 10)))
    return {
        "bg": colors.get("color0") or "#000",
        "title_bg": title_bg,

        "key": colors.get("color3") or "#aa0",
        "description": colors.get("color7") or "#fff",
        "title_fg": colors.get("color7") or "#fff",

    }


def data_to_object(data: str) -> dict:
    data_lines = [line.strip(" ") for line in data.split("\n") if line.strip(" ") != ""]

    keys_data = {}

    if len(data_lines) == 0:
        return keys_data

    for i, line in enumerate(data_lines):
        if line[0] == "*":
            keys_data["title"] = line[1:].strip(" ")
            continue

        key, *desc = line.split("-")
        if len(desc) == 0:
            raise HintsError(
                f"""Unexpected line at {i+1}

If thou wilt needs marry, marry a fool; for wise men know well enough what
monsters you make of them.
               
Maybe you forgot a '-' or a '*'"""
            )

        skey = key.strip(" ")
        sdesc = "-".join(desc).strip(" ")

        if len(skey) == 0:
            raise HintsError(f"Key expected at line: {i+1}\n\t{line}")
        if len(sdesc) == 0:
            raise HintsError(f"Description expected at line: {i+1}\n\t{line}")

        keys_data["keys"] = keys_data.get("keys", []) + [(skey, sdesc)]

    return keys_data


def key_description_lbl(
    master: tk.Tk | tk.Frame, key: str, description: str, colors: dict
) -> tk.Frame:
    """
    Generates a key-descrption Widget to give the possibility to use different
    colors for each element.
    """

    frame = tk.Frame(master)
    frame.configure(bg=colors["bg"])
    key_lbl = tk.Label(
        frame,
        text=key,
        fg=colors["key"],
        font=("Fira Code", 10),
        bg=colors["bg"],
    )
    description_lbl = tk.Label(
        frame,
        text="- " + description,
        fg=colors["description"],
        font=("Fira Code", 10),
        bg=colors["bg"],
    )

    key_lbl.pack(side=tk.LEFT)
    description_lbl.pack(side=tk.LEFT)

    return frame



def create_app(
    key_bindings: list,
    win_title: str,
):
    """
    Creates a Tkinter window that displays the keybind with their respective description as given.

    @param key_bindings:
    @param win_title: The title of the window, usualy the description of the KeyChord
    """

    # colors = get_xrdb_colors()

    colors = get_wal_colors()

    root = tk.Tk()
    root.title("Qtile hints")
    root.configure(bg=colors["bg"])

    tk.Label(
        root,
        text=win_title,
        font=("Fira Code", 14),
        fg=colors["title_fg"],
        bg=colors["title_bg"],
    ).pack(fill=tk.X)

    key_frame = tk.Frame(root)
    key_frame.configure(bg=colors["bg"])
    key_frame.pack()

    cols = 5
    row = 0
    col = 0
    for key in key_bindings:
        msg_lbl = key_description_lbl(
            master=key_frame, key=key[0], description=key[1], colors=colors
        )
        msg_lbl.grid(row=row, column=col, sticky=tk.W, ipadx=10)
        col += 1
        if col == cols:
            row += 1
            col = 0

    if col == 0:
        row -= 1

    height = 24 + (1 + row) * 18 + 20

    top = root.winfo_screenheight() - height
    root.geometry(f"{root.winfo_screenwidth()-20}x{height}+10+{top-10}")

    return root


def main():

    key_data = sys.stdin.read()

    keys = data_to_object(key_data)

    app = create_app(keys["keys"], keys["title"])

    app.mainloop()


if __name__ == "__main__":
    main()
