import unittest
import tkhints as th


class TestDataToObject(unittest.TestCase):
    def test_data_to_object(self):
        self.assertDictEqual(th.data_to_object("h-henlo"), {"keys": [("h", "henlo")]})

    def test_data_to_object_w_title(self):
        self.assertDictEqual(
            th.data_to_object("* Apustaja\nh-henlo"),
            {"title": "Apustaja", "keys": [("h", "henlo")]},
        )

    def test_data_to_object_w_nothing(self):
        self.assertDictEqual(th.data_to_object(""), {})

    def test_data_to_object_error(self):
        with self.assertRaises(th.HintsError):
            th.data_to_object("nu\nc-correct")
        with self.assertRaises(th.HintsError):
            th.data_to_object("* da\ngresit")
        with self.assertRaises(th.HintsError):
            th.data_to_object("* da\n-resit")
        with self.assertRaises(th.HintsError):
            th.data_to_object("* da\ng-")

    def test_data_to_object_with_minuses(self):
        self.assertDictEqual(
            th.data_to_object("h-henlo-frens"), {"keys": [("h", "henlo-frens")]}
        )

    def test_data_to_object_srip(self):
        self.assertDictEqual(
            th.data_to_object("h      -    henlo"), {"keys": [("h", "henlo")]}
        )


if __name__ == "__main__":
    unittest.main()
